"""A simple CNN model in Pytorch."""
import torch.nn as nn


class CNN(nn.Module):
    """A simple CNN model in Pytorch."""

    def __init__(self, num_classes=10):
        """Initialize CNN model."""
        super(CNN, self).__init__()
        self.layer1 = nn.Conv2d(1, 16, kernel_size=5, stride=1, padding=2)
        self.relu1 = nn.ReLU()
        self.layer2 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.layer3 = nn.Conv2d(16, 32, kernel_size=5, stride=1, padding=2)
        self.relu2 = nn.ReLU()
        self.layer4 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.fc = nn.Linear(7 * 7 * 32, num_classes)

    def forward(self, x):
        """Forward pass of CNN model."""
        out = self.layer1(x)
        out = self.relu1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.relu2(out)
        out = self.layer4(out)
        out = out.reshape(out.size(0), -1)
        out = self.fc(out)
        return out
