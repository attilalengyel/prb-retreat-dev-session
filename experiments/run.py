import wandb
import random


def main(args):

    wandb.init(
        # Set the project where this run will be logged
        project="my-awesome-project",
        # Track hyperparameters and run metadata
        config=args,
    )

    offset = random.random() / 5  # For demo only

    # Simulating a training run
    for epoch in range(2, args.epochs):

        loss = 2**-epoch + random.random() / epoch + offset
        acc = 1 - 2**-epoch - random.random() / epoch - offset
        # Tip: in real code, use torchmetrics to compute accuracy.

        print(f"epoch={epoch}, accuracy={acc}, loss={loss}")

        wandb.log({"accuracy": acc, "loss": loss})


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--lr", type=float, default=0.01)
    parser.add_argument("--epochs", type=int, default=10)
    args = parser.parse_args()

    main(args)
